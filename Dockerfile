FROM mongo:5.0.5
COPY ./init.sh /docker-entrypoint-initdb.d/init.sh
COPY ./mongo-scripts /mongo-scripts
