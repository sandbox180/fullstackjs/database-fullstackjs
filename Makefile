ifeq (build,$(firstword $(MAKECMDGOALS)))
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  $(eval $(RUN_ARGS):;@:)
endif


#============INSERT VALUE HERE ================

DOCKER_REGISTRY=registry.gitlab.com/sandbox180/fullstackjs/database-fullstackjs

#=============================================



#----------- Build -------------
IMAGE_NAME=$(DOCKER_REGISTRY)

.PHONY: build
.ONESHELL:
build:
	docker build --rm -t $(IMAGE_NAME):latest .
	[ ! -z  $(RUN_ARGS) ] && docker tag $(IMAGE_NAME):latest $(IMAGE_NAME):$(RUN_ARGS)
	docker push --all-tags $(IMAGE_NAME)
